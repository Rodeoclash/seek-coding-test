app-sh:
	docker-compose run app bash

setup:
	docker-compose run app yarn install

test:
	docker-compose run app make flow-check
	docker-compose run app make test-unit
