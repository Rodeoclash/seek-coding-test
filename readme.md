# Setup

1. Install docker and docker-compose
2. `make setup` to install dependencies
3. `make test` to run flow and unit tests
4. `docker-compose up` to start server. Server will be bound to your docker IP and available on port `8080`

Docker is not required to run the project, but will ease setup. To manually install, see steps run in `Dockerfile.dev` and follow those to configure your environment.

# Pricing engine

The bulk of the coding test seems to be around the pricing engine. In broad terms I've implemented it like this:

1. Each of the selected ad types are counted into a map (classic, standout, premium)
2. If an n for n discount applies, the counts are reduced by that ratio.
3. Prices are generated by finding the first pricing rule that matches the count (e.g. greater than equal to 3, price is $nn.nn). All clients have at least one entry that will be matched (greater or equal than 1). More specific rules should be placed at the start of the array.

I've treated all numbers as cents due to the way JS handles floats and can cause rounding errors. On a commercial project I would likely use specific decimal handling library.

# Development notes

* This project was adapted from an earlier project where I had a lot of code already setup (e.g. Docker, Makefiles), I don't want this to be an indication that I over engineer or over setup projects.

* In every project I try to incorporate something new to it. In this case I've wanted to learn https://www.styled-components.com/ for a while so I've used that here.

* I debated using plain React vs. React Mobx - the app is so simple that plain React would be fine to use. I've opted to include Mobx to demonstrate my knowledge of the library. This is only used to handle the state of the shopping cart.

* Data structure for pricing rules can be found in `services/pricing.js`, likewise unit tests for the rules can be found in `services/pricing.test.js`

* I have forgone integration tests. Normally I would use Nightwatch or Capybara or similar

* I've added aria-labels where I deem important.

* I've made some assumptions around the way the 2 for 3 deals work, mainly that they will continue to apply in multiples. E.g. 3 for the price of 2 will also be 6 for the price of 4. I'm sorry if that is incorrect, it's what "seemed" logical to me at the time. Normally I would check with the feature owner when implementing this.

* The coding test gave a suggested API implementation for adding products to the cart, I felt this didn't quite mesh with the way React / MobX worked so I have adjusted slighty (see `stores/cart.js` for implementation). Normally this would result in a small discussion between me and the owner of the ticket to see if we should adjust the implementation or not. I don't want to seem that I can't follow instructions, rather that I will see possible improvements and discuss them.

* The unit testing is pretty light on (only the pricing engine). Normally I would also unit test the other services and store pages. I would also test components that have local state but not usually functional component unless they had complex logic in them.
