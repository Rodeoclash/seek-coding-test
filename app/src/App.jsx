// @flow

import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'

import { Cart } from './App/Cart.jsx'
import { Clients } from './App/Clients.jsx'
import { Products } from './App/Products.jsx'

import 'normalize.css/normalize.css'
import 'milligram/dist/milligram.css'
import './App.css'

const mountNode = document.createElement('div')
mountNode.setAttribute('id', 'app')

if (!document.body) {
  throw new Error('Unable to mount application, no document body found')
}

document.body.appendChild(mountNode)

let Main = ({className}) => (
  <main className={className}>
    <div>
      <h3>Products</h3>
      <Products />
    </div>
    <div>
      <h3>Cart</h3>
      <Clients />
      <Cart />
    </div>
  </main>
)

Main = styled(Main)`
  background: #eee;
  display: flex;
  width: 50vw;

  > div {
    flex-grow: 1;
    padding: 1rem;
    width: 50%;
  }
`

ReactDOM.render(
  <Main />,
  mountNode,
)
