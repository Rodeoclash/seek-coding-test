// @flow

import R from 'ramda'
import { observable } from "mobx"
import {
  customerRules,
  calculatePrice,
} from 'services/pricing.js'

import type { ProductType } from 'services/product.js'

export const contents = observable({
  client: null,
  products: [],
})

export const setClient = (name: ?string) => {
  contents.client = name
}

export const add = (product: ProductType) => {
  contents.products.push(product)
}

export const remove = (idx: number) => {
  contents.products.splice(idx, 1)
}

export const total = () => {
  const ids = R.map(R.prop('id'))(contents.products)
  return calculatePrice(customerRules, ids, contents.client)
}

export const clear = () => {
  contents.products = []
}
