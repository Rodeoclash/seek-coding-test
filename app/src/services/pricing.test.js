import {
  applyRatio,
  calculateCountTotal,
  calculatePrice,
  countProducts,
  customerRules,
  findPredicate,
  parseRatio,
} from './pricing.js'

describe('.parseRatio', () => {
  it('should return discount and threshold', () => {
    const result = parseRatio('4:5')
    expect(result).toEqual([4, 5])
  })
})

describe('.findPredicate', () => {
  it('should return null with no matching predicate', () => {
    const result = findPredicate(customerRules.base.classic.predicates, -1)
    expect(result).toEqual(null)
  })

  it('should return a predicate matching the criteria', () => {
    const result = findPredicate(customerRules.base.classic.predicates, 4)
    expect(result).toEqual({kind: 'gte', value: 0, price: 26999})
  })

  it('should return the first matching criteria', () => {
    const result = findPredicate(customerRules.ford.premium.predicates, 4)
    expect(result).toEqual({kind: 'gte', value: 3, price: 38999})
  })
})

describe('.countProducts', () => {
  it('should calculate', () => {
    const result = countProducts(['classic', 'standout', 'premium'])
    expect(result).toEqual({classic: 1, standout: 1, premium: 1})
  })

  it('should calculate duplicates', () => {
    const result = countProducts(['classic', 'standout', 'premium', 'classic'])
    expect(result).toEqual({classic: 2, standout: 1, premium: 1})
  })

  it('should handle missing', () => {
    const result = countProducts(['classic', 'standout'])
    expect(result).toEqual({classic: 1, standout: 1, premium: 0})
  })
})

describe('.applyRatio', () => {
  it('should handle missing', () => {
    const result = applyRatio(customerRules.base, {classic: 1, standout: 1, premium: 1})
    expect(result).toEqual({classic: 1, standout: 1, premium: 1})
  })

  it('should reduce count when at ratio', () => {
    const result = applyRatio(customerRules.ford, {classic: 5, standout: 1, premium: 1})
    expect(result).toEqual({classic: 4, standout: 1, premium: 1})
  })

  it('should reduce count when at ratio', () => {
    const result = applyRatio(customerRules.unilever, {classic: 3, premium: 1})
    expect(result).toEqual({classic: 2, premium: 1})
  })

  it('should reduce count when greater than ratio', () => {
    const result = applyRatio(customerRules.ford, {classic: 7, standout: 1, premium: 1})
    expect(result).toEqual({classic: 6, standout: 1, premium: 1})
  })

  it('should reduce count when a multiple of ratio', () => {
    const result = applyRatio(customerRules.ford, {classic: 10, standout: 1, premium: 1})
    expect(result).toEqual({classic: 8, standout: 1, premium: 1})
  })

  it('should reduce count when greater than a multiple of ratio', () => {
    const result = applyRatio(customerRules.ford, {classic: 12, standout: 1, premium: 1})
    expect(result).toEqual({classic: 10, standout: 1, premium: 1})
  })

  it('should raise when not supplied a rule', () => {
    expect(() => {
      applyRatio(null, {classic: 12, standout: 1, premium: 1})
    }).toThrow()
  })

  it('should raise when not supplied a product without a matching rule', () => {
    expect(() => {
      applyRatio(customerRules.ford, {classic: 12, standout: 1, missing: 1})
    }).toThrow()
  })
})

describe('.calculateCountTotal', () => {
  it('should calculate total using base rules', () => {
    const result = calculateCountTotal(customerRules.base, {classic: 1, standout: 1, premium: 1})
    expect(result).toEqual(98797)
  })

  it('should calculate total using ford rules', () => {
    const result = calculateCountTotal(customerRules.ford, {premium: 4})
    expect(result).toEqual(155996)
  })

  it('should raise when not supplied a product without a matching rule', () => {
    expect(() => {
      calculateCountTotal(customerRules.ford, {missing: 4})
    }).toThrow()
  })
})

describe('.calculatePrice', () => {
  it('should match acceptance criteria for default', () => {
    const result = calculatePrice(customerRules, ['classic', 'standout', 'premium'])
    expect(result).toEqual(98797)
  })

  it('should match acceptance criteria for unilever', () => {
    const result = calculatePrice(customerRules, ['classic', 'classic', 'classic', 'premium'], 'unilever')
    expect(result).toEqual(93497)
  })

  it('should match acceptance criteria for apple', () => {
    const result = calculatePrice(customerRules, ['standout', 'standout', 'standout', 'premium'], 'apple')
    expect(result).toEqual(129496)
  })

  it('should match acceptance criteria for nike', () => {
    const result = calculatePrice(customerRules, ['premium', 'premium', 'premium', 'premium'], 'nike')
    expect(result).toEqual(151996)
  })
})
