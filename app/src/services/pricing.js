// @flow

import R from 'ramda'

import type { ProductIdType } from './product.js'

type CustomersType = 'base'|'unilever'|'apple'|'nike'|'ford'

type AdCountType = {
  classic: number,
  premium: number,
  standout: number,
}

type PredicateType = {
  kind: 'gte',
  value: number,
  price: number,
}

type CustomerRuleType = {
  [key: string]: {
    ratio?: string,
    predicates: Array<PredicateType>,
  },
}

type CustomerRulesType = {
  [key: string]: CustomerRuleType,
}

export const customerRules: CustomerRulesType = {
  base: {
    classic: {
      predicates: [
        {kind: 'gte', value: 0, price: 26999},
      ],
    },
    standout: {
      predicates: [
        {kind: 'gte', value: 0, price: 32299},
      ],
    },
    premium: {
      predicates: [
        {kind: 'gte', value: 0, price: 39499},
      ],
    },
  },
  unilever: {
    classic: {
      ratio: '3:2',
      predicates: [
        {kind: 'gte', value: 0, price: 26999},
      ],
    },
    standout: {
      predicates: [
        {kind: 'gte', value: 0, price: 32299},
      ],
    },
    premium: {
      predicates: [
        {kind: 'gte', value: 0, price: 39499},
      ],
    },
  },
  apple: {
    classic: {
      predicates: [
        {kind: 'gte', value: 0, price: 26999},
      ],
    },
    standout: {
      predicates: [
        {kind: 'gte', value: 0, price: 29999},
      ],
    },
    premium: {
      predicates: [
        {kind: 'gte', value: 0, price: 39499},
      ],
    },
  },
  nike: {
    classic: {
      predicates: [
        {kind: 'gte', value: 0, price: 26999},
      ],
    },
    standout: {
      predicates: [
        {kind: 'gte', value: 0, price: 29999},
      ],
    },
    premium: {
      predicates: [
        {kind: 'gte', value: 4, price: 37999},
        {kind: 'gte', value: 0, price: 39499},
      ],
    },
  },
  ford: {
    classic: {
      ratio: '5:4',
      predicates: [
        {kind: 'gte', value: 0, price: 26999},
      ],
    },
    standout: {
      predicates: [
        {kind: 'gte', value: 0, price: 30999},
      ],
    },
    premium: {
      predicates: [
        {kind: 'gte', value: 3, price: 38999},
        {kind: 'gte', value: 0, price: 39499},
      ],
    },
  },
}

/*
 * @function parseRatio
 *
 * Given a ratio string in the format "n:n" will return a tuple containing the threshold and discount
 */
export const parseRatio = R.pipe(
    R.split(':'),
    R.map(R.unary(parseInt)),
  )

/**
 * @function findPredicate
 *
 * Given an array of predicates (from the rules map) will return the first matching predicate or null if none was found.
 *
 * Will raise if an unknown predicate is requested.
 *
 * @param { Predicate[] } predicates - The array of predicates to search with
 * @param { number } count - The count of product items to use when matching the predicate
 */
export const findPredicate = (predicates: Array<PredicateType>, count: number): ?PredicateType =>
  R.find((predicate: PredicateType) => {
    switch (predicate.kind) {
      case 'gte':
        return count >= predicate.value
      default:
        throw new Error('Unknown predicate kind')
    }
  })(predicates) || null

export const countProducts = (products: Array<ProductIdType>): AdCountType =>
  R.reduce((acc, product: ProductIdType): AdCountType => {
    acc[product] += 1
    return acc
  }, {
    classic: 0,
    premium: 0,
    standout: 0,
  })(products)

const getCustomerRuleOrError = (customerRule: CustomerRuleType, product: string) => {
  const customerRuleProduct = customerRule[product]

  if (!customerRuleProduct) {
    throw new Error('No product found in rule, ensure it is defined in the pricing rules')
  }

  return customerRuleProduct
}

/**
 * @function applyRatio
 *
 * Given a customer rule map and a counts map (in the form {adType: n}) will discount / reduce the counts based on the rule
 *
 * To apply the discount, a ratio key must be present on the rule.
 *
 * @param { CustomerRule } customerRule - A customer rule map (e.g. unilever or ford)
 * @counts { AdCount} counts - A count of advert types in a map (e.g. {classic: 1, standout: 2})
 */
export const applyRatio = R.curry((customerRule: CustomerRuleType, counts: AdCountType): AdCountType  => {

  if (!customerRule) {
    throw new Error('No customer rule found, ensure it is defined in the pricing rules')
  }

  return R.pipe(
    R.toPairs,
    R.map(([product, count]: [ProductIdType, number]): [ProductIdType, number] => {
      const customerRuleProduct = getCustomerRuleOrError(customerRule, product)

      if (!customerRuleProduct.ratio) {
        return [product, count]
      }

      const [threshold, discount] = parseRatio(customerRuleProduct.ratio)
      const discountAmount = threshold - discount
      const discountCount = Math.floor(count / threshold)

      return [product, count - discountAmount * discountCount]
    }),
    R.fromPairs,
  )(counts)
})

/**
 * @function calculateCountTotal
 *
 * Given a customer rule map and a counts map (in the form {adType: n}) will calculate the total price of the adverts
 *
 * @param { CustomerRule } customerRule - A customer rule map (e.g. unilever or ford)
 * @counts { AdCount} counts - A count of advert types in a map (e.g. {classic: 1, standout: 2})
 */
export const calculateCountTotal = R.curry((customerRule: CustomerRuleType, counts: AdCountType): number =>
  R.pipe(
    R.toPairs,
    R.reduce((acc, [product, count]: [ProductIdType, number]): number  => {
      const customerRuleProduct = getCustomerRuleOrError(customerRule, product)
      const predicate = findPredicate(customerRuleProduct.predicates, count)

      if (!predicate) {
        throw new Error('Unable to find predicate for price rule, ensure at least one rule always matches as a default')
      }

      acc += predicate.price * count

      return acc
    }, 0)
  )(counts)
)

/*
 * @funcion calculatePrice
 *
 * A convinence method that given rules, a string array of products and an optional customer name, will find the correct rule, count the products, apply discount ratios to the counts and return the total.
 *
 * @param {CustomerRules} customerRule - A collection of customer rules. Specific rule set will be selected with the optional customer param.
 * @param {ProductId[]} products - A an array of product ids (e.g. [classic, standout, standout, premium]
 * @param {string} customer - Optional name of customer to select the rules from. This will default to base if none are selected.
 */
export const calculatePrice = (
  customerRules: CustomerRulesType,
  products: Array<ProductIdType>,
  customer?: string,
): number => {
  const rules = customerRules[customer || 'base']

  return R.pipe(
    countProducts,
    applyRatio(rules),
    calculateCountTotal(rules),
  )(products)
}
