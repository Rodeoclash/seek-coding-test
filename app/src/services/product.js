// @flow

export type ProductIdType = 'classic'|'standout'|'premium'

export type ProductType = {
  id: ProductIdType,
  name: string,
}

export const products: Array<ProductType> = [
  {
    id: 'classic',
    name: 'Classic Ad',
  },
  {
    id: 'standout',
    name: 'Standout Ad',
  },
  {
    id: 'premium',
    name: 'Premium Ad',
  },
]
