// @flow

import React from 'react'

import R from 'ramda'

import { clients } from 'services/client.js'
import { setClient } from 'stores/cart.js'

import { Client } from './Clients/Client.jsx'

type PropsType = {}

const renderItem = R.curry((props: PropsType, client: string): React.Element<*> => (
  <Client
    key={client}
    client={client}
  />
))

const handleSubmit = (event) => {
  event.preventDefault()
}

const handleChange = (event: SyntheticInputEvent) => {
  const {
    target: {
      value,
    },
  } = event

  setClient(value || null)
}

export const Clients = (props: PropsType): React.Element<*> => {
	const renderedItems = R.into([])(
		R.map(renderItem(props)),
		clients.sort(),
	)

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor='selectClient'>
        Client
      </label>
      <select
        id='selectClient'
        onChange={handleChange}
      >
        <option value=''>Default</option>
        {renderedItems}
      </select>
    </form>
  )
}
