// @flow

import React from 'react'

import R from 'ramda'

import { products } from 'services/product.js'

import { Product } from './Products/Product.jsx'

import type { ProductType } from 'services/product.js'

type PropsType = {}

const renderItem = R.curry((props: PropsType, product: ProductType): React.Element<*> => (
  <Product
    key={product.id}
    product={product}
  />
))

const sort = R.sortWith([
  R.ascend(R.prop('name')),
])

export const Products = (props: PropsType): React.Element<*> => {
	const renderedItems = R.into([])(
		R.map(renderItem(props)),
		sort(products),
	)

  return (
    <div>
      {renderedItems}
    </div>
  )
}
