// @flow

import React from 'react'

import { clients } from 'services/client.js'

type PropsType = {
  client: string,
}

export const Client = (props: PropsType): React.Element<*> => {
  const {
    client,
  } = props

  return (
    <option
      value={client}
    >
      {client}
    </option>
  )
}
