// @flow

import React from 'react'

import R from 'ramda'
import {
  clear,
  contents,
  total,
} from 'stores/cart.js'
import { observer } from 'mobx-react'
import styled from 'styled-components'

import { Product } from './Cart/Product.jsx'

import type { ProductType } from 'services/product.js'

type PropsType = {}

const renderItem = R.curry((props: PropsType, product: ProductType, idx): React.Element<*> => {
  return (
    <Product
      key={idx}
      idx={idx}
      product={product}
    />
  )
})

const mapI = R.addIndex(R.map)

const handleClick = (event: SyntheticMouseEvent) => {
  clear()
}

export const Cart = observer((props: PropsType): React.Element<*> => {
  const renderedItems = R.pipe(
		mapI(renderItem(props)),
  )(contents.products)

  contents.client

  let Total = ({className}) => (
    <p className={className}>
      ${total() / 100}
    </p>
  )

  Total = styled(Total)`
    border-top: 3px double #ccc;
  `

  if (renderedItems.length === 0) {
    return (
      <p>
        <em>Please add a product on the left</em>
      </p>
    )
  }

  return (
    <div>
      {renderedItems}
      <Total />
      <button onClick={handleClick}>
        Clear cart
      </button>
    </div>
  )
})
