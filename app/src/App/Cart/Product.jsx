// @flow

import React from 'react'

import R from 'ramda'
import styled from 'styled-components'
import { remove } from 'stores/cart.js'

import type {
  ProductType,
} from 'services/product.js'

type PropsType = {
  className?: string,
  idx: number,
  product: ProductType,
}

const handleClick = R.curry((props: PropsType, product: ProductType, event: SyntheticMouseEvent) => {
  const {
    idx,
  } = props

  remove(idx)
})

export let Product = (props: PropsType): React.Element<*> => {
  const {
    className = '',
    product,
  } = props

  return (
    <a
      aria-label={`Remove product "${product.name}" from the cart`}
      className={className}
      href='javascript://'
      onClick={handleClick(props, product)}
    >
      {product.name}
    </a>
  )
}

Product = styled(Product)`
  display: block;
`
