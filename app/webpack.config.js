var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    app: './src/App.jsx',
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, '/build'),
    publicPath: '/assets/',
    sourceMapFilename: '[name].js.map',
  },
  devServer: {
    disableHostCheck: true,
    host: '0.0.0.0',
    noInfo: true,
    quiet: false,
    stats: {
      colors: true,
      errorDetails: true
    },
  },
  devtool: 'eval',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve('./src'),
      'node_modules',
    ],
  },
}
